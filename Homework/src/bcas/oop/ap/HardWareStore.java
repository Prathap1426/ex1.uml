package bcas.oop.ap;

public class HardWareStore {

	private String partNumber;
	private String partDescription;
	private int quantityOfItemPurchased;
	private double pricePerItem;

	public HardWareStore(String partNumber, String partDescription, int quantityOfItemPurchased, double pricePerItem) {

		this.partNumber = partNumber;
		this.partDescription = partDescription;
		this.quantityOfItemPurchased = quantityOfItemPurchased;
		this.pricePerItem = pricePerItem;
	}

	public String getPartNumber() {
		return partNumber;
	}

	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}

	public String getPartDescription() {
		return partDescription;
	}

	public void setPartDescription(String partDescription) {
		this.partDescription = partDescription;
	}

	public int getQuantityOfItemPurchased() {
		return quantityOfItemPurchased;
	}

	public void setQuantityOfItemPurchased(int quantityOfItemPurchased) {
		this.quantityOfItemPurchased = quantityOfItemPurchased;
	}

	public double getPricePerItem() {
		return pricePerItem;
	}

	public void setPricePerItem(double pricePerItem) {
		this.pricePerItem = pricePerItem;
	}

	public double gethardwarestoreAmount() {
		double calculateTotalAmount;
		calculateTotalAmount = quantityOfItemPurchased * pricePerItem;
		return calculateTotalAmount;

	}
}
