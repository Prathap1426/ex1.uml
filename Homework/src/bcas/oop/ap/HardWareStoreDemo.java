package bcas.oop.ap;

import java.util.Scanner;

public class HardWareStoreDemo {

	public static void main(String[] args) {

		String partNumber = null;
		String partDescription = null;
		int quantityOfItemPurchased = 0;
		double pricePerItem = 0;
		HardWareStore hardWareStore = new HardWareStore(partNumber, partDescription, quantityOfItemPurchased,
				pricePerItem);
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter part no - ");
		partNumber = scan.nextLine();
		hardWareStore.setPartNumber(partNumber);

		System.out.println("Enter the partDescription - ");
		partDescription = scan.nextLine();
		hardWareStore.setPartDescription(partDescription);

		System.out.println("Enter the quantityOfItemPurchased  - ");
		quantityOfItemPurchased = scan.nextInt();
		hardWareStore.setQuantityOfItemPurchased(quantityOfItemPurchased);

		System.out.println("pricePerItem - ");
		pricePerItem = scan.nextDouble();
		hardWareStore.setPricePerItem(pricePerItem);

		System.out.println(hardWareStore.gethardwarestoreAmount());
	}

}
