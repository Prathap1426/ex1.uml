package bcas.oop.ap.ul;

public abstract class Coke extends ColdDrink {
	public abstract String name();

	public abstract String size();

	public abstract float price();

}
