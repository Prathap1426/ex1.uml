package bcas.oop.ap.ul;

public abstract class Pepsi extends ColdDrink {
	public abstract String name();

	public abstract String size();

	public abstract float price();

}
