package bcas.oop.ap.ul;

public abstract class NonVegPizza extends Pizza {
	public abstract String name();

	public abstract String size();

	public abstract float price();

}
